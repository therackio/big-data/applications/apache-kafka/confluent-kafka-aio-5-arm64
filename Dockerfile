FROM phusion/baseimage:latest-arm64 as scratch

ARG CONFLUENT_VERSION
ARG SCALA_VERSION

# Temp unboxing
RUN \
    set eux && \
    export DEBIAN_FRONTEND=noninteractive && apt update && apt install -y wget && \
    export major=`echo $CONFLUENT_VERSION | cut -d. -f1` && export minor=`echo $CONFLUENT_VERSION | cut -d. -f2` && export CONFLUENT_VERSION_MAJOR=`echo $major.$minor` && \
    echo $CONFLUENT_VERSION_MAJOR && \
    wget http://packages.confluent.io/archive/$CONFLUENT_VERSION_MAJOR/confluent-community-$CONFLUENT_VERSION-$SCALA_VERSION.tar.gz && \
    tar -xzf confluent-community-$CONFLUENT_VERSION-$SCALA_VERSION.tar.gz && \
    rm -rf confluent-community-$CONFLUENT_VERSION-$SCALA_VERSION.tar.gz && \
    ls -al


# Final Image
FROM registry.gitlab.com/therackio/base-images/openjdk-zulu-arm64:stretch-11

ARG CONFLUENT_VERSION
ENV CONFLUENT_VERSION $CONFLUENT_VERSION

ARG SCALA_VERSION

ENV TERM=xterm

LABEL maintainer="gitlab@therack.io"
LABEL description="Confluent Platform with Community components"
LABEL confluent_version="${CONFLUENT_VERSION}"
LABEL scala_version="${SCALA_VERSION}"

COPY --from=scratch /confluent-$CONFLUENT_VERSION /confluent-$CONFLUENT_VERSION

RUN \
    set eux && \
    apt-get -qq update && \
    apt-get -qq -y --no-install-recommends install curl bash && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    ls -al && \
    ls -al /confluent-$CONFLUENT_VERSION && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV CONFLUENT_HOME="/confluent-$CONFLUENT_VERSION"
ENV PATH="${CONFLUENT_HOME}/bin:${PATH}"

CMD ["bash"]
