# confluent-kafka-arm64

"All In One" application image for Confluent Platform using only Confluent Community components, setup generally as described here: https://docs.confluent.io/current/installation/installing_cp/zip-tar.html#prod-kafka-cli-install